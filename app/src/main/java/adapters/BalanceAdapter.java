package adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


import com.orhanobut.hawk.Hawk;
import com.test.borya.FullOrderActivity;
import com.test.borya.R;

import java.util.ArrayList;


import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import models.BalanceObject;
import models.OrdersObject;

public class BalanceAdapter extends RecyclerView.Adapter<BalanceAdapter.ViewHolder> implements View.OnClickListener {
    private ArrayList<BalanceObject> arrayObjectsBalance;
    Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView cardBalanceInfo;
        public TextView cardBalanceValue;
        public TextView cardBalanceDate;
        private FrameLayout cardFrame;
        public ViewHolder(CardView v) {
            super(v);
            cardBalanceInfo = v.findViewById(R.id.cardBalanceInfo);
            cardBalanceValue = v.findViewById(R.id.cardBalanceValue);
            cardBalanceDate = v.findViewById(R.id.cardBalanceDate);
            cardFrame = v;
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public BalanceAdapter(ArrayList<BalanceObject> arrayObjectsOrdersConsrtuctor, Context context) {
        arrayObjectsBalance = arrayObjectsOrdersConsrtuctor;
        this.context=context;
    }
    @Override
    public void onClick(View view) {
        // Hawk.put("cardObject",view.getTag());
        int clickPos = Integer.valueOf(view.getTag().toString());
        if(arrayObjectsBalance.get(clickPos).statusBalance.equals("transaction")){


        Hawk.put("broadcastToFullId",arrayObjectsBalance.get(clickPos).postIdTransaction); //не трогать


        context.startActivity((new Intent(context, FullOrderActivity.class)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        //  toastMsgLg("view.getTag().toString()");
            }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public BalanceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // create a new view
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card_balance, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String userId = Hawk.get("userIdHawk");
        holder.cardBalanceDate.setText(arrayObjectsBalance.get(position).dateBalance);
        holder.cardBalanceValue.setText(arrayObjectsBalance.get(position).moneyValue+" €");
        switch (arrayObjectsBalance.get(position).statusBalance){
            case "new":
                holder.cardBalanceInfo.setText(context.getResources().getString(R.string.adapter_deposit_status_consid));
                break;
            case "canceled":
                holder.cardBalanceInfo.setText(context.getResources().getString(R.string.adapter_deposit_status_reject));
                break;
            case "approuve":
                holder.cardBalanceInfo.setText(context.getResources().getString(R.string.adapter_deposit_status_success));
                holder.cardBalanceValue.setText("+"+arrayObjectsBalance.get(position).moneyValue+" €");
                holder.cardBalanceValue.setTextColor(ContextCompat.getColor(context, R.color.colorGr));
                break;
            case "transaction":
                holder.cardFrame.setOnClickListener(this);
                holder.cardFrame.setTag(String.valueOf(position));
                if(arrayObjectsBalance.get(position).ownerOrderBalance.equals(userId)){
                    //зачисление за заказ
                    holder.cardBalanceInfo.setText(context.getResources().getString(R.string.adapter_deposit_crediting));
                    holder.cardBalanceDate.setText(arrayObjectsBalance.get(position).dateBalance);
                    holder.cardBalanceValue.setText("+"+arrayObjectsBalance.get(position).moneyValue+" €");
                    holder.cardBalanceValue.setTextColor(ContextCompat.getColor(context, R.color.colorGr));
                }
                if(arrayObjectsBalance.get(position).accepterOrderBalance.equals(userId)) {
                    //списание за заказ
                    holder.cardBalanceInfo.setText(context.getResources().getString(R.string.adapter_deposit_charge));
                    holder.cardBalanceDate.setText(arrayObjectsBalance.get(position).dateBalance);
                    holder.cardBalanceValue.setText("-"+arrayObjectsBalance.get(position).moneyValue+" €");
                    holder.cardBalanceValue.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                }
                break;
        }

    }
    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return arrayObjectsBalance.size();
    }




}
