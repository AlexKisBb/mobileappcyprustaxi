package adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.orhanobut.hawk.Hawk;
import com.test.borya.FullOrderActivity;
import com.test.borya.R;

import java.util.ArrayList;


import androidx.appcompat.widget.AppCompatButton;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import models.OrderModel;
import models.OrdersObject;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> implements View.OnClickListener {
    private ArrayList<OrderModel> arrayObjectsOrders;
    Context context;
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public AppCompatButton btnFull;
        public TextView txtFrom;
        public TextView txtTo;
        public TextView textOwner;
        public TextView txtPrice;
        public TextView  cardTextMonth;
        public TextView  cardTextTime;
        public TextView  cardTextStatus;
        public TextView textIdOrder;
        public ImageView  imgAvatar;
        public View  viewLine;
        private FrameLayout cardFrame;
        public ViewHolder(CardView v) {
            super(v);
            btnFull = v.findViewById(R.id.btnFull);
            txtFrom = v.findViewById(R.id.cardTextFrom);
            txtTo = v.findViewById(R.id.cardTextTo);
            textOwner = v.findViewById(R.id.textOwner);
            txtPrice = v.findViewById(R.id.price);
            cardTextMonth = v.findViewById(R.id.cardTextMonth);
            cardTextTime = v.findViewById(R.id.cardTextTime);
            imgAvatar = v.findViewById(R.id.cardAvatar);
            cardTextStatus = v.findViewById(R.id.textStatus);
            viewLine = v.findViewById(R.id.viewLine);
            textIdOrder = v.findViewById(R.id.textIdOrder);
            cardFrame = v;
        }
    }
    public OrdersAdapter(ArrayList<OrderModel> arrayObjectsOrdersConsrtuctor, Context context) {
        arrayObjectsOrders = arrayObjectsOrdersConsrtuctor;
        this.context=context;
    }
    @Override
    public void onClick(View view) {
        int clickPos = Integer.valueOf(view.getTag().toString());
        Hawk.put("broadcastToFullId",arrayObjectsOrders.get(clickPos).id);
       //проверка откуда был совершен переход
        Hawk.put("checkIntent","main");
       context.startActivity((new Intent(context, FullOrderActivity.class)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
      //  toastMsgLg("view.getTag().toString()");
    }

    // Create new views (invoked by the layout manager)
    @Override
    public OrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        // create a new view
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        String result1 = arrayObjectsOrders.get(position).pointA.split("/")[0];
        holder.txtFrom.setText(result1);
        String result2 = arrayObjectsOrders.get(position).pointB.split("/")[0];
        holder.txtTo.setText(result2);
        holder.txtPrice.setText(arrayObjectsOrders.get(position).priceOrder+"€ / "+ arrayObjectsOrders.get(position).price+"€");


        holder.textOwner.setText(arrayObjectsOrders.get(position).ownerOrder);
        //создание короткого id
        String sevenCharsId = "id:"+(arrayObjectsOrders.get(position).id).substring((arrayObjectsOrders.get(position).id).length()-7); ;
        holder.textIdOrder.setText(sevenCharsId);
        Glide.with(context)
                .asBitmap()
                .load( arrayObjectsOrders.get(position).ownerAvatar)
                .apply(RequestOptions.circleCropTransform())
                .into(holder.imgAvatar);

        String[] parts = arrayObjectsOrders.get(position).date.split(" в ");
        String part1 = parts[0]; // 004
        String part2 = parts[1];
        holder.cardTextMonth.setText(part1);
        holder.cardTextTime.setText(part2);
        holder.btnFull.setOnClickListener(this);
        holder.btnFull.setTag(String.valueOf(position));
        if((Hawk.get("userIdHawk")).equals( arrayObjectsOrders.get(position).ownerId)){
            ViewGroup.LayoutParams params = holder.cardFrame.getLayoutParams();
            params.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 220, context.getResources().getDisplayMetrics());
            holder.cardFrame.setLayoutParams(params);
            holder.imgAvatar.setVisibility(View.VISIBLE);
            holder.textOwner.setVisibility(View.VISIBLE);
            holder.viewLine.setVisibility(View.VISIBLE);
            holder.textIdOrder.setVisibility(View.VISIBLE);
        }

        }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return arrayObjectsOrders.size();
    }

    public void toastMsgLg(String msg){
        Toast toast = Toast.makeText(context,
                msg, Toast.LENGTH_LONG);
        toast.show();
    }
}
