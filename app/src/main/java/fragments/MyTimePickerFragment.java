package fragments;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.fragment.app.DialogFragment;
import eventbus.DatePickerEvent;
import eventbus.TimePickerEvent;

public class MyTimePickerFragment extends DialogFragment {
    String formattedTime;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        Date date=c.getTime();
        java.text.DateFormat dateFormat = new SimpleDateFormat("h:mm a");
        formattedTime = dateFormat.format(date);

        return new TimePickerDialog(getActivity(), timeSetListener, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    private TimePickerDialog.OnTimeSetListener timeSetListener =
            new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    TimePickerEvent event = new TimePickerEvent();
                    String mSt;
                    mSt = String.valueOf(minute);
                    if(minute<10){
                        mSt = "0"+mSt;
                    }

                    String date  = hourOfDay+":"+mSt;

                        event.setEventString(date);
                        EventBus.getDefault().post(event);
                }

            };
}
