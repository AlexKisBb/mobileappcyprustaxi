package fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import androidx.fragment.app.DialogFragment;
import eventbus.DatePickerEvent;

public class MyDatePickerFragment extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        Date date=c.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
      //  String formattedDatet = dateFormat.format(date);

        return new DatePickerDialog(getActivity(), dateSetListener, year, month, day);
    }

    private DatePickerDialog.OnDateSetListener dateSetListener =
            new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int month, int day) {
                    DatePickerEvent event = new DatePickerEvent();
                    String mSt;
                    month = month+1;
                    mSt = String.valueOf(month);
                    if(month<10){
                        mSt = "0"+mSt;
                    }
                    String date  = day+"."+mSt+"."+year;

                    event.setEventString(date);
                    EventBus.getDefault().post(event);
                }
            };

}
