package eventbus;

public class DatePickerEvent {
    private String eventString;

    public String getEventString() {
        return eventString;
    }

    public void setEventString(String eventString) {
        this.eventString = eventString;
    }
}
