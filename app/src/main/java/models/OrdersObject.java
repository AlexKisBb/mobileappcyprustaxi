package models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrdersObject {
    @SerializedName("Id")
    @Expose
    public String id;
    @SerializedName("ServiceCategoryCode")
    @Expose
    public String categoryCode;
    @SerializedName("PaymentTypeCode")
    @Expose
    public String payCode;
    @SerializedName("Title")
    @Expose
    public String title;
    @SerializedName("DateTimeBegin")
    @Expose
    public String timeBegin;
    @SerializedName("Price")
    @Expose
    public String price;
    @SerializedName("OrderStatusCode")
    @Expose
    public String orderStatusCode;


}
