package models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderModel {
    @SerializedName("pointA")
    @Expose
    public String pointA;
    @SerializedName("pointB")
    @Expose
    public String pointB;
    @SerializedName("date")
    @Expose
    public String date;
    @SerializedName("amountPeople")
    @Expose
    public String amountPeople;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("comment")
    @Expose
    public String comment;
    @SerializedName("babyChair")
    @Expose
    public String babyChair;
    @SerializedName("babySmallChair")
    @Expose
    public String babySmallChair;
    @SerializedName("ownerOrder")
    @Expose
    public String ownerOrder;
    @SerializedName("firslLt")
    @Expose
    public String firslLt;
    @SerializedName("firslLg")
    @Expose
    public String firslLg;
    @SerializedName("secondLt")
    @Expose
    public String secondLt;
    @SerializedName("secondLg")
    @Expose
    public String secondLg;
    @SerializedName("_id")
    @Expose
    public String id;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("accepterId")
    @Expose
    public String accepterId;
    @SerializedName("ownerAvatar")
    @Expose
    public String ownerAvatar;
    @SerializedName("accepter")
    @Expose
    public String accepter;
    @SerializedName("phoneClient")
    @Expose
    public String phoneClient;
    @SerializedName("ownerId")
    @Expose
    public String ownerId;
    @SerializedName("priceOrder")
    @Expose
    public String priceOrder;
    @SerializedName("unixMcOrder")
    @Expose
    public String unixMcOrder;
    @SerializedName("blMoneyFromTheCreator")
    @Expose
    public String blMoneyFromTheCreator;

}
