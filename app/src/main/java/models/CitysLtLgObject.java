package models;

public class CitysLtLgObject {
    //  Лимасол, Ларнака, Ларнака аэропорт, Пафос, Пафос аэропорт, Никосия, Ая-напа, Проторас
    String limassolLt = "34.683534";
    String limassolLg = "33.0226142";

    String larnacaLt = "34.9013534";
    String larnacaLg = "33.5501945";

    String larnacaAirLt = "34.8723402";
    String larnacaAirLg = "33.6181629";

    String paphosLt = "34.7723942";
    String paphosLg = "32.4092349";

    String paphosAirLt = "34.7123004";
    String paphosAirLg = "32.4865204";

    String nicossiaLt = "35.1922475";
    String nicossiaLg = "33.3623827";

    String ayaNapaLt = "34.9855509";
    String ayaNapaLg = "33.9749187";

    String protorasLt = "35.0040158";
    String protorasLg = "34.0249585";

    public String getLimassolLt() {
        return limassolLt;
    }

    public String getLimassolLg() {
        return limassolLg;
    }

    public String getLarnacaLt() {
        return larnacaLt;
    }

    public String getLarnacaLg() {
        return larnacaLg;
    }

    public String getLarnacaAirLt() {
        return larnacaAirLt;
    }

    public String getLarnacaAirLg() {
        return larnacaAirLg;
    }

    public String getPaphosLt() {
        return paphosLt;
    }

    public String getPaphosLg() {
        return paphosLg;
    }

    public String getPaphosAirLt() {
        return paphosAirLt;
    }

    public String getPaphosAirLg() {
        return paphosAirLg;
    }

    public String getNicossiaLt() {
        return nicossiaLt;
    }

    public String getNicossiaLg() {
        return nicossiaLg;
    }

    public String getAyaNapaLt() {
        return ayaNapaLt;
    }

    public String getAyaNapaLg() {
        return ayaNapaLg;
    }

    public String getProtorasLt() {
        return protorasLt;
    }

    public String getProtorasLg() {
        return protorasLg;
    }
}
