package models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TokenModel {
    @SerializedName("token")
    @Expose
    public String token;

    @SerializedName("namesurname")
    @Expose
    public String namesurname;

    @SerializedName("pathphoto")
    @Expose
    public String pathphoto;

    @SerializedName("_id")
    @Expose
    public String id;

    @SerializedName("userAvatar")
    @Expose
    public String userAvatar;

    @SerializedName("userStatus")
    @Expose
    public String userStatus;

    @SerializedName("userBalance")
    @Expose
    public String userBalance;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("userBalanceBlock")
    @Expose
    public String userBalanceBlock;



}
