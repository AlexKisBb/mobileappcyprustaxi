package models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BalanceObject {
    @SerializedName("moneyValue")
    @Expose
    public String moneyValue;
    @SerializedName("dateBalance")
    @Expose
    public String dateBalance;
    @SerializedName("statusBalance")
    @Expose
    public String statusBalance;
    @SerializedName("userId")
    @Expose
    public String userId;
    @SerializedName("accepterOrderBalance")
    @Expose
    public String accepterOrderBalance;
    @SerializedName("ownerOrderBalance")
    @Expose
    public String ownerOrderBalance;
    @SerializedName("postIdTransaction")
    @Expose
    public String postIdTransaction;
}
