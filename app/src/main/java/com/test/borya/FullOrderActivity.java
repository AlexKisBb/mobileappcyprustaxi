package com.test.borya;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.ContextCompat;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.orhanobut.hawk.Hawk;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import models.OrderModel;
import models.TokenModel;
import network.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FullOrderActivity extends AppCompatActivity implements OnMapReadyCallback {

    GoogleMap gMap;
    MapView mView;
    public TextView txtFrom;
    public TextView txtTo;
    public TextView textOwner;
    public TextView txtPrice;
    public TextView  txtBabyChair;
    public TextView  txtPeople;
    public TextView  txtComment;
    public FrameLayout frameBabyChair;
    public FrameLayout frameSmallBabyChair;
    public FrameLayout frameMoneyFromTheCreator;
    public TextView  txtDate;
    public TextView  txtTime;
    AppCompatButton takeOrderBtn;
    AppCompatButton verificationOrderBtn;
    FrameLayout fullOrderClientPhoneFrame;
    TextView fullOrderClientPhoneTxt;

    String pointAHawk;
    String pointBHawk;
    String dateHawk;
    String timeHawk;
    String ownerHawk;
    String accepterHawk;
    String babyChairHawk;
    String frameSmallBabyChairHawk;
    String commentHawk;
    String peopleHawk;


    String firstLt;
    String firstLg;
    String secondLt;
    String secondLg;
    String postId;
    String ownerUsername;
    String userAvatarLink;
    ImageView userAvatar;
    String orderStatusHawk;
    String moneyFromTheCreator;

    String statusCancellation;

    String priceHawk;
    String priceFullHawk;
    String userId;
    String userIdForB;
    String userBalance;

    String formattedDate;

    AlertDialog.Builder alertDialog;

    String ownerId;
    View lineOwner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_order);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        mView = (MapView)findViewById(R.id.mapView) ;
        mView.onCreate(savedInstanceState);
        mView.onResume();
        mView.getMapAsync(this);

        txtFrom = (TextView)findViewById(R.id.fullOrderPointA);
        txtTo = (TextView)findViewById(R.id.fullOrderPointB);
        txtPrice  = (TextView)findViewById(R.id.fullOrderPrice);
        textOwner  = (TextView)findViewById(R.id.fullOrderOwner);
        txtBabyChair = (TextView)findViewById(R.id.fullOrderBabyChair);
        txtPeople = (TextView)findViewById(R.id.fullOrderPeople);
        frameBabyChair = (FrameLayout)findViewById(R.id.frameBabyChair);
        frameSmallBabyChair = (FrameLayout)findViewById(R.id.frameSmallBabyChair);
        txtComment = (TextView)findViewById(R.id.fullOrderComment) ;
        txtDate = (TextView)findViewById(R.id.fullOrderDate) ;
        txtTime  = (TextView)findViewById(R.id.fullOrderTime) ;
        takeOrderBtn = (AppCompatButton)findViewById(R.id.takeOrderBtn) ;
        userAvatar = (ImageView)findViewById(R.id.userAvatar) ;
        verificationOrderBtn = (AppCompatButton)findViewById(R.id.verificationOrderBtn) ;
        fullOrderClientPhoneFrame = (FrameLayout) findViewById(R.id.fullOrderClientPhoneFrame) ;
        fullOrderClientPhoneTxt = (TextView) findViewById(R.id.fullOrderClientPhoneTxt) ;
        frameMoneyFromTheCreator = (FrameLayout)findViewById(R.id.frameMoneyFromTheCreator) ;
        lineOwner = (View) findViewById(R.id.lineOwner);
        Intent intent = getIntent();

        //проверка откуда был совершен переход, отображение создателя заказа
        String checkIntentHawk = Hawk.get("checkIntent");
        switch (checkIntentHawk){
            case "story":
                lineOwner.setVisibility(View.VISIBLE);
                textOwner.setVisibility(View.VISIBLE);
                userAvatar.setVisibility(View.VISIBLE);
                break;
            case "main":
                lineOwner.setVisibility(View.GONE);
                textOwner.setVisibility(View.GONE);
                userAvatar.setVisibility(View.GONE);
                break;
        }

        ownerUsername = Hawk.get("usernameHawk");//не трогать
        postId = Hawk.get("broadcastToFullId");
        getOneOrderOnStart();
//  end OnCreate -----------------------------------------------------------------------------------
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(this);
        gMap = googleMap;

    }

    public void clickBack (View v){
        finish();
    }

    public void toastMsgLg(String msg){
        Toast toast = Toast.makeText(getApplicationContext(),
                msg, Toast.LENGTH_LONG);
        toast.show();
    }

    public void methodAcceptOrder(){
//        switch (takeOrderBtn.getText().toString()){
//            case "отменить":
//                statusChange ();
//                finish();
//                break;
//            case "принять":
//                getOneOrder();
//                break;
//        }
        if(takeOrderBtn.getText().toString().equals(getResources().getString(R.string.fullorder_activity_btn_dec))){
            statusChange ();
            finish();
        }
        if(takeOrderBtn.getText().toString().equals(getResources().getString(R.string.fullorder_activity_btn_acc))){
            getOneOrder();
        }
       // acceptOrder ();
    }
    public void methodVerification(){
        statusChangeFinish();
        finish();
    }
    public void getOneOrder(){
        ApiClient.getClient().getOneOrder(postId).enqueue(new Callback<OrderModel>() {
            @Override
            public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
            if(response.body().status != null && response.body().status.equals("new")){
                getUserBalance ();
            }
            else {
                toastMsgLg(getResources().getString(R.string.fullorder_activity_or_dec));
            }
            }
            @Override
            public void onFailure(Call<OrderModel> call, Throwable t) {

            }
        });
    }
    public void acceptOrder (){
        if(userBalance != null && Integer.valueOf(userBalance)>=Integer.valueOf(priceHawk)){

        userId = Hawk.get("userIdHawk");
        String userName = Hawk.get("usernameHawk");
        String status = "inwork";
        ApiClient.getClient().putAcceptOrder(postId,userId,userName,status).enqueue(new Callback<OrderModel>() {
            @Override
            public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
                transferPrice();
            }
            @Override
            public void onFailure(Call<OrderModel> call, Throwable t) {

            }});        }
        else {
            toastMsgLg(getResources().getString(R.string.fullorder_activity_insuf));
        }
    }
    public void statusChange(){
        String status = statusCancellation;
        ApiClient.getClient().putStatusChangeOrder(postId,status).enqueue(new Callback<OrderModel>() {
            @Override
            public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
                toastMsgLg(getResources().getString(R.string.fullorder_activity_ord_dec));
                finish();
            }
            @Override
            public void onFailure(Call<OrderModel> call, Throwable t) {

            }});
    }
    public void statusChangeFinish(){
        String status = "finished";
        ApiClient.getClient().putFinishedOrder(postId,status).enqueue(new Callback<OrderModel>() {
            @Override
            public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
                toastMsgLg(getResources().getString(R.string.fullorder_activity_or_fin));
                finish();
            }
            @Override
            public void onFailure(Call<OrderModel> call, Throwable t) {

            }});
    }

    public void showAlertAcceptOrder(View v){
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(getResources().getString(R.string.fullorder_activity_app_act));

        //case "принять"
        String approve = getResources().getString(R.string.fullorder_activity_btn_acc);
        //case "отменить"
        String decline = getResources().getString(R.string.fullorder_activity_btn_dec);
//        switch (takeOrderBtn.getText().toString()){
//            case "принять":
//                alertDialog.setMessage("После принятия заказа, вам станет доступен номер телефона клиента(История заказов -> Принятые мной -> Подробнее), комиссия будет списана после принятия автоматически. " +
//                        "Вы подтверждаете что хотите взять данный заказ?");
//                break;
//            case "отменить":
//                alertDialog.setMessage("Вы действительно хотите отменить данный заказ?");
//                break;
//        }
        if(takeOrderBtn.getText().toString().equals(approve)){
            alertDialog.setMessage(getResources().getString(R.string.fullorder_activity_app_info));
        }
        if(takeOrderBtn.getText().toString().equals(decline)){
            alertDialog.setMessage(getResources().getString(R.string.fullorder_activity_quest));
        }


        // задаем иконку
       // alertDialog.setIcon(R.drawable.delete);
        // Обработчик на нажатие ДА
        alertDialog.setPositiveButton(getResources().getString(R.string.fullorder_activity_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                // Код который выполнится после закрытия окна
    //-------------принятие либо отмена с проверкой блокировки пользователя-------------------------
                getUserStatus ();
            }
        });
        // Обработчик на нажатие НЕТ
        alertDialog.setNegativeButton(getResources().getString(R.string.fullorder_activity_no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Код который выполнится после закрытия окна
                dialog.cancel();
            }
        });
        // показываем Alert
        alertDialog.show();
    }

    //подтверждение выполнения заказа --------------------------------------------------------------
    public void showAlertVerification(View v){
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(getResources().getString(R.string.fullorder_activity_app_act));
        // Указываем Title
       // alertDialog.setTitle("Подтвердить удаление...");
        // Указываем текст сообщение
        alertDialog.setMessage(getResources().getString(R.string.fullorder_activity_app_ord_qu));
        // задаем иконку
        // alertDialog.setIcon(R.drawable.delete);
        // Обработчик на нажатие ДА
        alertDialog.setPositiveButton(getResources().getString(R.string.fullorder_activity_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                // Код который выполнится после закрытия окна
                methodVerification();
            }
        });
        // Обработчик на нажатие НЕТ
        alertDialog.setNegativeButton(getResources().getString(R.string.fullorder_activity_no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Код который выполнится после закрытия окна
                dialog.cancel();
            }
        });
        // показываем Alert
        alertDialog.show();
    }
    //END подтверждение выполнения заказа ----------------------------------------------------------

    public void transferPrice(){

        ApiClient.getClient().postTransferOderPrice(userId,ownerId,priceHawk).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                postBalanceTransaction ("transaction","transaction",priceHawk,"transaction",formattedDate,userId,ownerId,postId);
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }});
    }

    public void getUserBalance (){
        userIdForB = Hawk.get("userIdHawk");
        ApiClient.getClient().getOneUser(userIdForB).enqueue(new Callback<TokenModel>() {
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                userBalance = response.body().userBalance;
                acceptOrder ();
            }
            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
                toastMsgLg(t.toString());
            }
        });
    }

    public void getOneOrderOnCreate(){
        ApiClient.getClient().getOneOrder(postId).enqueue(new Callback<OrderModel>() {
            @Override
            public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
                if(response.body().status != null && response.body().status.equals("new")){
                    getUserBalance ();
                }
                else {
                    toastMsgLg(getResources().getString(R.string.fullorder_activity_or_dec));
                }
            }
            @Override
            public void onFailure(Call<OrderModel> call, Throwable t) {

            }
        });
    }

//-- onCreate logic --------------------------------------------------------------------------------
    public void getOneOrderOnStart(){
        ApiClient.getClient().getOneOrder(postId).enqueue(new Callback<OrderModel>() {
            @Override
            public void onResponse(Call<OrderModel> call, Response<OrderModel> response) {
               // if(response.body().status != null && response.body().status.equals("new")){
                    pointAHawk = response.body().pointA;
                    pointBHawk = response.body().pointB;
                   // String timeHawk = response.body().;
                    String[] parts = response.body().date.split(getResources().getString(R.string.fullorder_activity_in));
                    String part1 = parts[0]; // 004
                    String part2 = parts[1];
                    dateHawk = part1;
                    timeHawk = part2;
                    priceHawk = response.body().price;
                    priceFullHawk = response.body().priceOrder;
                    ownerHawk = response.body().ownerOrder;
                    //String ownerUsername = Hawk.get("usernameHawk");
                    orderStatusHawk = response.body().status;
                    ownerId = response.body().ownerId;
                    accepterHawk = response.body().phoneClient;
                    babyChairHawk = response.body().babyChair;
                    frameSmallBabyChairHawk = response.body().babySmallChair;
                    commentHawk = response.body().comment;
                    peopleHawk = response.body().amountPeople;
                    firstLt = response.body().firslLt;
                    firstLg = response.body().firslLg;
                    secondLt = response.body().secondLt;
                    secondLg = response.body().secondLg;
                    postId = response.body().id;
                    userAvatarLink = response.body().ownerAvatar;
                    moneyFromTheCreator = response.body().blMoneyFromTheCreator;
                    onCrLogicTomethod ();
            }
            @Override
            public void onFailure(Call<OrderModel> call, Throwable t) {

            }
        });
    }
//---END onCreate logic ----------------------------------------------------------------------------

    public void postBalanceTransaction (String id,String name,String money,String status,String date,String accepterOrderBalance,String iownerOrderBalanced,String postIdBalanceTrance){
        ApiClient.getClient().postBalanceReq(id,name,money,status,date,accepterOrderBalance,iownerOrderBalanced,postIdBalanceTrance).enqueue(new Callback<Void>(){
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                //toastMsgLg(String.valueOf(response.code()));
                toastMsgLg(getResources().getString(R.string.fullorder_activity_or_acc));
                startActivity((new Intent(getApplicationContext(), StoryOrdersActivity.class)).putExtra("intentname", "accept").setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
             //   Intent intent = getIntent();
              //  toastMsgLg(intent.getStringExtra("intentname"));
                finish();
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }
    //получение текущей даты
    public void getCurrentDate (){
        Calendar cal = Calendar. getInstance();
        Date date=cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy 'в' HH:mm");
        formattedDate = dateFormat.format(date);
    }

    public void onCrLogicTomethod (){
        txtFrom.setText(pointAHawk.split("/")[0]);
        txtTo.setText(pointBHawk.split("/")[0]);
        //проверяем статус для соответствующего отображения кнопок
        // скрываем кнопки если статусы не соответствующие
        if(orderStatusHawk.equals("finished") || orderStatusHawk.equals("cancellationAcceptor") || orderStatusHawk.equals("cancellationOwner") ){
            takeOrderBtn.setVisibility(View.GONE);
            verificationOrderBtn.setVisibility(View.GONE);
        }
        else {
            if(ownerHawk.equals(ownerUsername)){
                //показываем номер клиента создателю заказа
                fullOrderClientPhoneFrame.setVisibility(View.VISIBLE);
                if(orderStatusHawk.equals("new") || orderStatusHawk.equals("onmoderation")){
                    takeOrderBtn.setVisibility(View.VISIBLE);
                    takeOrderBtn.setText(getResources().getString(R.string.fullorder_activity_btn_dec));
                    statusCancellation = "cancellationOwner";
                }
                else {
                    takeOrderBtn.setVisibility(View.GONE);
                }
            }
            if(!ownerHawk.equals(ownerUsername)){
                if(orderStatusHawk.equals("new") ){
                    takeOrderBtn.setText(getResources().getString(R.string.fullorder_activity_btn_acc));
                }
                else {
                    takeOrderBtn.setVisibility(View.VISIBLE);
                    takeOrderBtn.setText(getResources().getString(R.string.fullorder_activity_btn_dec));
                    statusCancellation = "cancellationAcceptor";
                    verificationOrderBtn.setVisibility(View.VISIBLE);
                    fullOrderClientPhoneFrame.setVisibility(View.VISIBLE);
                    // if(Hawk.get("broadcastToFullOrderPhoneClient")!= null){
                    //String accepterHawk = Hawk.get("broadcastToFullOrderPhoneClient");   //поменять
                    fullOrderClientPhoneTxt.setText(accepterHawk);
                    txtFrom.setText(pointAHawk);
                    txtTo.setText(pointBHawk);
                    // }
                }
            }
        }

        txtPrice.setText(priceFullHawk+"€ / "+priceHawk+"€");
        textOwner.setText(ownerHawk);
        txtPeople.setText(peopleHawk + getResources().getString(R.string.fullorder_activity_fare));
        switch (babyChairHawk){
            case "yes":
                frameBabyChair.setVisibility(View.VISIBLE);
                break;
            case "no":
                frameBabyChair.setVisibility(View.GONE);
                break;
        }
        switch (frameSmallBabyChairHawk){
            case "yes":
                frameSmallBabyChair.setVisibility(View.VISIBLE);
                break;
            case "no":
                frameSmallBabyChair.setVisibility(View.GONE);
                break;
        }
        switch (moneyFromTheCreator){
            case "true":
                frameMoneyFromTheCreator.setVisibility(View.VISIBLE);
                break;
            case "false":
                frameMoneyFromTheCreator.setVisibility(View.GONE);
                break;
        }
        txtComment.setText(commentHawk);
        txtDate.setText(dateHawk);
        txtTime.setText(timeHawk);
        Glide.with(getApplicationContext())
                .asBitmap()
                .load(userAvatarLink)
                .apply(RequestOptions.circleCropTransform())
                .into(userAvatar);
        getCurrentDate ();

        gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        CameraPosition camPos = CameraPosition.builder().target(new LatLng(Double.valueOf(firstLt),Double.valueOf(firstLg))).zoom(7).bearing(0).tilt(45).build();
        gMap.moveCamera(CameraUpdateFactory.newCameraPosition(camPos));
        MarkerOptions marker = new MarkerOptions().position(new LatLng(Double.valueOf(firstLt),Double.valueOf(firstLg))).title("Hello Maps");
        gMap.addMarker(marker);
        MarkerOptions marker2 = new MarkerOptions().position(new LatLng(Double.valueOf(secondLt),Double.valueOf(secondLg))).title("Hello Maps");
        gMap.addMarker(marker2);
    }

    //проверка на предмет блокировки перед принятием либо отменой заказа ----------------------------
    public void getUserStatus (){
        String userIdForB = Hawk.get("userIdHawk");
        ApiClient.getClient().getOneUser(userIdForB).enqueue(new Callback<TokenModel>() {
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                String userStatus = response.body().userStatus;
                if(!userStatus.equals("userIsBlocked")){
                    //принятие либо отмена заказа
                    methodAcceptOrder();
                }
                else{
                    Hawk.put("tokenPutSmsAct",null);
                    toastMsgLg(getResources().getString(R.string.fullorder_activity_acc_block));
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                }
            }
            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
                toastMsgLg(t.toString());
            }
        });
    }


}
