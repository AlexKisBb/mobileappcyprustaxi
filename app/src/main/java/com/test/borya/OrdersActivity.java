package com.test.borya;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.orhanobut.hawk.Hawk;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Collections;

import adapters.OrdersAdapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import eventbus.DatePickerEvent;
import models.OrderModel;
import models.OrdersObject;
import network.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView mRecyclerView;
    private OrdersAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<OrderModel> arrayObjectsOrders= new ArrayList<>();
    ArrayList<OrderModel> arrayObjectsOrdersSorted= new ArrayList<>();
    SwipeRefreshLayout  swipeRefresh;
    TextView textNoOrders;
    FrameLayout progresFrame;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_orders);

        View layout = getLayoutInflater().inflate(R.layout.nav_header_orders,null);

        View headerView = navigationView.getHeaderView(0);

        ImageView imgAvatar = (ImageView) headerView.findViewById(R.id.imageViewNav);
        TextView txtNameDrawer = (TextView) headerView.findViewById(R.id.navHeaderName);
        TextView txtMailDrawer = (TextView) headerView.findViewById(R.id.textMailDrawer);
        progresFrame = (FrameLayout) headerView.findViewById(R.id.progresFrame);
        textNoOrders = (TextView) findViewById(R.id.textNoOrders);

        String ownerMail = Hawk.get("ownerMail");
        txtMailDrawer.setText(ownerMail);

        userId = Hawk.get("userIdHawk");

        String ownerName = Hawk.get("usernameHawk");
        txtNameDrawer.setText(ownerName);

        //фото пользователя в дровере
        String pathImgAvatar = Hawk.get("pathImg");
        Glide.with(getApplicationContext())
                .asBitmap()
                .load( pathImgAvatar)
                .apply(RequestOptions.circleCropTransform())
                .into(imgAvatar);

        recyclerInit ();
        getOrderss ();

        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeRefresh.setOnRefreshListener(this);

        firebaseGetToken ();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.orders, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_plus_order) {
            startActivity(new Intent(getApplicationContext(),CreateOrderActivity.class));
        } else if (id == R.id.nav_orders_history) {
            startActivity(new Intent(getApplicationContext(),StoryOrdersActivity.class));
        } else if (id == R.id.nav_cash) {
            startActivity(new Intent(getApplicationContext(),BalanceActivity.class));
        } else if (id == R.id.nav_call) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:+35799449021"));
            startActivity(intent);
        } else if (id == R.id.nav_exit) {
            Hawk.deleteAll();
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    //инициализация RecyclerView
    public void recyclerInit (){
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        // specify an adapter (see also next example)
        mAdapter = new OrdersAdapter(arrayObjectsOrdersSorted,getApplicationContext());
        mRecyclerView.setAdapter(mAdapter);
    }

    public void clickToFullOrder(View v){
       // startActivity(new Intent(getApplicationContext(),FullOrderActivity.class).putExtra("extraOrdersActivity", "extraOrdersActivity"));
    }
    public void getOrderss (){
        ApiClient.getClient().getOrders().enqueue(new Callback<ArrayList<OrderModel>>() {
            @Override
            public void onResponse(Call<ArrayList<OrderModel>> call, Response<ArrayList<OrderModel>> response) {
                arrayObjectsOrders.clear();
                arrayObjectsOrders = response.body();
                Collections.reverse(arrayObjectsOrders);
                arrayObjectsOrdersSorted.clear();
                for(int i = 0;i<arrayObjectsOrders.size();i++){
                    if(arrayObjectsOrders.get(i).status.equals("new")){
                        arrayObjectsOrdersSorted.add(arrayObjectsOrders.get(i));
                    }
                }
                //   toastMsg(String.valueOf(arrayObjectsOrders.size()));
                if(arrayObjectsOrdersSorted.size() == 0){
                    textNoOrders.setVisibility(View.VISIBLE);
                }
                else {
                    textNoOrders.setVisibility(View.GONE);
                }
                recyclerInit ();
            }
            @Override
            public void onFailure(Call<ArrayList<OrderModel>> call, Throwable t) {
                toastMsgLg(t.toString());
            }
        });
    }

    public void toastMsgLg(String msg){
        Toast toast = Toast.makeText(getApplicationContext(),
                msg, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void onRefresh() {
        // Отменяем анимацию обновления
        getOrderss ();
        swipeRefresh.setRefreshing(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getOrderss ();
    }

    //получение fTokena
    public void firebaseGetToken (){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                          //  Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        sendFtoken (token);
                        // Log and toast
                      //  String msg = getString("ffff", token);
                       // Log.d(TAG, msg);
                        Log.wtf("dddssse",token);
                       // Toast.makeText(OrdersActivity.this, token, Toast.LENGTH_SHORT).show();
                    }
                });
    }
    //отправка fTokena на сервер
    public void sendFtoken (String fToken){
        ApiClient.getClient().putfToken(userId,fToken).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code() == 200 || response.code() == 204 ){
                  //  toastMsgLg("Токен отправлен");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }




}
