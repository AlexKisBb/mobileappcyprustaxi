package com.test.borya;

import android.app.Application;

import com.orhanobut.hawk.Hawk;

public class AppClass extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Hawk.init(this).build();
}
}
