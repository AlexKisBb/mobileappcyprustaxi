package com.test.borya;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.nguyenhoanglam.imagepicker.model.Config;
import com.nguyenhoanglam.imagepicker.model.Image;
import com.nguyenhoanglam.imagepicker.ui.imagepicker.ImagePicker;
import com.orhanobut.hawk.Hawk;

import java.io.File;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import models.TokenModel;
import network.ApiClient;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnterPersonalInformation extends AppCompatActivity {
    CircularImageView userImageView;
    TextView addPhotoText;
    String userId;
    String email;
    String pass;
    String repeatPass;
    String namesurname;
    String phone;
    String carmark;
    String carmodel;
    TextInputEditText  editNameSurname;
    TextInputEditText  editEmail;
    TextInputEditText  editPhone;
    TextInputEditText  editPassword;
    TextInputEditText  editTextCarMark;
    TextInputEditText  editTextCarModel;
    TextInputEditText  editRepeatPassword;
    FrameLayout prgFrame;
    Spinner spinnerClassAuto;
    String path;
    private static final String BASE_URL="http://main-lb-748297709.us-east-2.elb.amazonaws.com/";
    AlertDialog.Builder alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_personal_information);
        userImageView = findViewById(R.id.circle_img);
        addPhotoText = findViewById(R.id.addPhotoText);
        prgFrame = findViewById(R.id.progres_frame);
        editNameSurname = findViewById(R.id.editNameSurname);
        editEmail = findViewById(R.id.editEmail);
        editPhone = findViewById(R.id.editPhone);
        editPassword = findViewById(R.id.editPassword);
        editTextCarMark = findViewById(R.id.editTextCarMark);
        editTextCarModel = findViewById(R.id.editTextCarModel);
        spinnerClassAuto = findViewById(R.id.spinnerClassAuto);
        editRepeatPassword = findViewById(R.id.editRepeatPassword);
    }

    public void clickAddPhoto (View v){
        addPhotoText.setVisibility(View.GONE);
        ImagePicker.with(this)
                .setFolderMode(true)// folder mode (false by default)
                .setFolderTitle("Folder") // folder selection title
                .setImageTitle("Tap to select") // image selection title// single mode
                .setShowCamera(true) // show camera or not (true by default)
                .setFolderMode(true) // directory name for captured image  ("Camera" folder by default)
                .start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //получение выбраного города
        if (requestCode == Config.RC_PICK_IMAGES && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(Config.EXTRA_IMAGES);
            // do your logic here...

            path = images.get(0).getPath();
            Hawk.put("pathImg",path);
            Glide.with(getApplicationContext())
                    .asBitmap()
                    .load(path)
                    .apply(RequestOptions.circleCropTransform())
                    .into(userImageView);
        }
    }

    public void toastMsgLg(String msg){
        Toast toast = Toast.makeText(getApplicationContext(),
                msg, Toast.LENGTH_LONG);
        toast.show();
    }
    public void clickBack (View v){
        finish();
    }
    //отправка регистрационных данных
    public void clickNextScreen (View v){
        if(path == null){
            toastMsgLg(getResources().getString(R.string.enter_user_info_choose_ph));
            return;
        }
        email = editEmail.getText().toString();
        pass = editPassword.getText().toString();
        repeatPass = editRepeatPassword.getText().toString();
        namesurname = editNameSurname.getText().toString();
        phone = editPhone.getText().toString();
        carmark = editTextCarMark.getText().toString();
        carmodel = editTextCarModel.getText().toString();
        char[] charArrayfemail = email.toCharArray();
        char[] charArraylpass = pass.toCharArray();
        char[] charArraynamesurname = namesurname.toCharArray();
        char[] charArrayphone = phone.toCharArray();
        char[] charArraycarmark = carmark.toCharArray();
        char[] charArraycarmodel = carmodel.toCharArray();
        char[] charArrayRepeatPass = repeatPass.toCharArray();

        if(charArrayfemail.length>2&charArraylpass.length>2&charArraynamesurname.length>2&charArrayphone.length>2&
                charArraycarmark.length>2&charArraycarmodel.length>1 & charArrayRepeatPass.length>2){
            if(charArraylpass.length == charArrayRepeatPass.length){

            }
            else {
                toastMsgLg(getResources().getString(R.string.enter_user_info_pass_no));
                return;
            }
            if(email.contains("@")&
                    email.contains(".")){
                prgFrame.setVisibility(View.VISIBLE);
                postUserAvatar ();
            }
            else {
                toastMsgLg(getResources().getString(R.string.enter_user_toast_em));
            }
        }
        else {
            toastMsgLg(getResources().getString(R.string.enter_user_toast_fill_all)); }
  }
//"application/x-www-form-urlencoded",
    public void sendRegistrationInfo(String photopath){
        String userStatus = "userOnModeration";
        ApiClient.getClient().postRegistrationData(email,pass,namesurname,phone,carmark,carmodel,photopath,userStatus).enqueue(new Callback<TokenModel>(){
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {

                prgFrame.setVisibility(View.GONE);
                userId = response.body().id;
                //toastMsgLg(String.valueOf(userId));
                firebaseGetToken ();
                Hawk.put("ownerName",namesurname);
                Hawk.put("ownerMail",email);
                showAlertAcceptRegistration();


            }
            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
                prgFrame.setVisibility(View.GONE);
            }
        });
    }
    public void postUserAvatar (){
        File imageFile = new File(path);
        RequestBody requestImageFile = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
        MultipartBody.Part photoMultipart =
                MultipartBody.Part.createFormData("file", imageFile.getName(), requestImageFile);

        ApiClient.getClient().postUserAvatar(photoMultipart).enqueue(new Callback<TokenModel>(){
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
            if(response.code() == 200){
            sendRegistrationInfo(BASE_URL+response.body().pathphoto);
            }
             else {
                prgFrame.setVisibility(View.GONE);
                 toastMsgLg(getResources().getString(R.string.enter_user_toast_ph_size));
            }
            }
            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
                prgFrame.setVisibility(View.GONE);
                toastMsgLg(t.toString());
                finish();
            }
        });
    }

    public void showAlertAcceptRegistration(){
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);
        alertDialog.setTitle(getResources().getString(R.string.enter_user_toast_action));

                alertDialog.setMessage(getResources().getString(R.string.enter_user_toast_reg_suc));

        alertDialog.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                // Код который выполнится после закрытия окна
                finish();
            }
        });
        // показываем Alert
        alertDialog.show();
    }
    //получение fTokena
    public void firebaseGetToken (){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            //  Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        sendFtoken (userId, token);
                        // Log and toast
                        //  String msg = getString("ffff", token);
                        // Log.d(TAG, msg);
                        Log.wtf("dddssse",token);
                        // Toast.makeText(OrdersActivity.this, token, Toast.LENGTH_SHORT).show();
                    }
                });
    }
    //отправка fTokena на сервер
    public void sendFtoken (String useId, String fToken){
        ApiClient.getClient().putfToken(useId,fToken).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code() == 200 || response.code() == 204 ){
                    //  toastMsgLg("Токен отправлен");
                }
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }

}
