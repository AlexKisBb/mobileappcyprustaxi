package com.test.borya;

import adapters.BalanceAdapter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import adapters.OrdersAdapter;
import models.BalanceObject;
import models.OrderModel;
import models.OrdersObject;
import models.TokenModel;
import network.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.orhanobut.hawk.Hawk;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

public class BalanceActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    Button btnProfit;
    Button btnExpense;
    TextView textSum;
    private RecyclerView mRecyclerView;
    private BalanceAdapter balanceAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<BalanceObject> arrayObjectsBalanceReq= new ArrayList<>();
    ArrayList<BalanceObject> arrayBalanceReqSorted= new ArrayList<>();
    CardView cardDialog;
    CardView cardDialog2;
    CardView cardWithdrawalInfo;
    CardView cardWithdrawalRequisites;
    RelativeLayout cardValue;
    RelativeLayout cardCardNumber;
    EditText editMoneyValue;
    String formattedDate;
    TextView textDepositEnterCard;
    SwipeRefreshLayout swipeBalance;

    //окно ввода реквизитов
    TextInputEditText editWithdrawalValue;
    TextInputEditText editCardWithdrawal;
    int valueSum;

    //заблокированые средства
    TextView textBalanceBlockCount;

    //получаем email пользователя вместе с балансом
    String userEmail;
    String userNameSurname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance_new);
        btnProfit = findViewById(R.id.btnProfit);
        btnExpense = findViewById(R.id.btnExpense);
        textSum =  findViewById(R.id.text_balance_count);
        //список
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_balance);
        cardDialog = (CardView) findViewById(R.id.cardDialog);
        cardDialog2 = (CardView) findViewById(R.id.cardDialog2);
        cardWithdrawalInfo = (CardView) findViewById(R.id.cardWithdrawalInfo);
        cardWithdrawalRequisites = (CardView) findViewById(R.id.cardWithdrawalRequisites);
        cardValue  = (RelativeLayout) findViewById(R.id.cardValue);
        cardCardNumber = (RelativeLayout) findViewById(R.id.cardCardNumber);
        editMoneyValue  = (EditText) findViewById(R.id.editMoneyValue);
        textDepositEnterCard  = (TextView) findViewById(R.id.text_deposit_enter_card);

        editWithdrawalValue = findViewById(R.id.editWithdrawalValue);
        editCardWithdrawal = findViewById(R.id.editWithdrawalCard);
        textBalanceBlockCount = findViewById(R.id.text_balance_block_count);


        swipeBalance = (SwipeRefreshLayout) findViewById(R.id.swipeBalance);
        swipeBalance.setOnRefreshListener(this);

        getUserBalance ();
        recyclerInit ();
        getBalanceReq ();
        getCurrentDate ();
        checkRequestBalance ();
    }
    public void recyclerInitBalanceRecords (){
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        //инициализируе адаптер
        balanceAdapter = new BalanceAdapter(arrayObjectsBalanceReq,getApplicationContext());
        mRecyclerView.setAdapter(balanceAdapter);
    }
    //balance new ----------------------------------------------------------------------------------
    //запрос на пополнение -> появление диалогового окна
    public void requestRefill(View v){
        cardDialog.setVisibility(View.VISIBLE);
        editMoneyValue.requestFocus();
          InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
          imm.showSoftInput(editMoneyValue, InputMethodManager.SHOW_IMPLICIT);
    }
    public void clickNextToCardNumber(View v){
        if(!editMoneyValue.getText().toString().equals("") && editMoneyValue.getText().toString() != null){
            hideSoftKeyboard(editMoneyValue);
            cardDialog.setVisibility(View.GONE);
            cardDialog2.setVisibility(View.VISIBLE);
            textDepositEnterCard.setText(getResources().getString(R.string.dialog_deposit_info_part1)+ editMoneyValue.getText().toString() +getResources().getString(R.string.dialog_deposit_info_part2));
            Hawk.put("checkRequestBalance", true);
            Hawk.put("checkRequestBalanceValue",editMoneyValue.getText().toString());
        }
        else {
            toastMsgLg(getResources().getString(R.string.activity_deposit_toast_summ));
        }
      //
      //  InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
      //  imm.showSoftInput(editMoneyValue, InputMethodManager.SHOW_IMPLICIT);
    }
    public void clickSendValueAndCardNumber(View v){
        //проверка статуса пользователя на предмет блокировки
        getUserStatus ();
    }
    //логика отправки запроса на пополнение
    public void sendBalanceValue (){
        String userId = Hawk.get("userIdHawk");
        String userName = Hawk.get("usernameHawk");
        String money;
        if(Hawk.get("checkRequestBalance") != null && Hawk.get("checkRequestBalance").equals(true)){
            money = Hawk.get("checkRequestBalanceValue");
        }else {
            money = editMoneyValue.getText().toString();
        }
        String status = "new";
        getCurrentDate ();
        postBalanceRequest(userId,userName,money,status,formattedDate);
        Hawk.put("checkRequestBalance", false);
        cardDialog.setVisibility(View.GONE);
        cardDialog2.setVisibility(View.GONE);
    }

    public void postBalanceRequest(String id,String name,String money,String status,String date){
        String fakeUser = "fakeUser";
        String fakeId = "fakeId";
        ApiClient.getClient().postBalanceReq(id,name,money,status,date,fakeUser,fakeUser,fakeId).enqueue(new Callback<Void>(){
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                recyclerInit ();
                getBalanceReq ();
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }

    public void toastMsgLg(String msg){
        Toast toast = Toast.makeText(getApplicationContext(),
                msg, Toast.LENGTH_LONG);
        toast.show();
    }
    //получение текущей даты
    public void getCurrentDate (){
        Calendar cal = Calendar.getInstance();
        Date date=cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy 'в' HH:mm");
        formattedDate = dateFormat.format(date);
    }

    public void clickBack (View v){
        finish();
    }

    //получение массива апросов на пополнение
    public void getBalanceReq (){
        ApiClient.getClient().getBalanceReq().enqueue(new Callback<ArrayList<BalanceObject>>() {
            @Override
            public void onResponse(Call<ArrayList<BalanceObject>> call, Response<ArrayList<BalanceObject>> response) {
                arrayObjectsBalanceReq.clear();
                arrayObjectsBalanceReq = response.body();
                Collections.reverse(arrayObjectsBalanceReq);
                arrayBalanceReqSorted.clear();
                String userId = Hawk.get("userIdHawk");
                for(int i = 0;i<arrayObjectsBalanceReq.size();i++){
                    if(arrayObjectsBalanceReq.get(i).userId.equals(userId) ||
                            arrayObjectsBalanceReq.get(i).accepterOrderBalance.equals(userId) ||
                            arrayObjectsBalanceReq.get(i).ownerOrderBalance.equals(userId)){
                        arrayBalanceReqSorted.add(arrayObjectsBalanceReq.get(i));
                    }
                }
                //   toastMsg(String.valueOf(arrayObjectsOrders.size()));
                recyclerInit ();
            }
            @Override
            public void onFailure(Call<ArrayList<BalanceObject>> call, Throwable t) {
                toastMsgLg(t.toString());
            }
        });
    }

    //инициализация RecyclerView
    public void recyclerInit (){
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)

        balanceAdapter = new BalanceAdapter(arrayBalanceReqSorted,getApplicationContext());
        mRecyclerView.setAdapter(balanceAdapter);
    }

    public void getUserBalance (){
        String userId = Hawk.get("userIdHawk");
        ApiClient.getClient().getOneUser(userId).enqueue(new Callback<TokenModel>() {
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                textSum.setText(response.body().userBalance + " €");
                textBalanceBlockCount.setText(response.body().userBalanceBlock + " €");
                valueSum = Integer.valueOf(response.body().userBalance);
                userEmail = response.body().email;
                userNameSurname = response.body().namesurname;
            }
            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
                toastMsgLg(t.toString());
            }
        });
    }

    public void clickPaySend (View v){
        String url = "https://paysend.com/";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @Override
    public void onRefresh() {
        getUserBalance ();
        recyclerInit ();
        getBalanceReq ();
        getCurrentDate ();
        swipeBalance.setRefreshing(false);
    }

    public void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public void dialogClose(View v){
        hideSoftKeyboard(editMoneyValue);
        cardDialog.setVisibility(View.GONE);
        cardDialog2.setVisibility(View.GONE);
        Hawk.put("checkRequestBalance", false);
    }

    public void checkRequestBalance (){
        if(Hawk.get("checkRequestBalance") != null && Hawk.get("checkRequestBalance").equals(true)){
            cardDialog2.setVisibility(View.VISIBLE);
            textDepositEnterCard.setText(getResources().getString(R.string.dialog_deposit_info_part1)+ Hawk.get("checkRequestBalanceValue") +getResources().getString(R.string.dialog_deposit_info_part2));
        }
    }

    public void getUserStatus (){
        String userIdForB = Hawk.get("userIdHawk");
        ApiClient.getClient().getOneUser(userIdForB).enqueue(new Callback<TokenModel>() {
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                String userStatus = response.body().userStatus;
                if(!userStatus.equals("userIsBlocked")){
                    sendBalanceValue ();
                }
                else{
                    Hawk.put("tokenPutSmsAct",null);
                    toastMsgLg(getResources().getString(R.string.activity_deposit_toast_block));
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                }
            }
            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
                toastMsgLg(t.toString());
            }
        });
    }

    public void requestWithdrawalScr1(View v){
        cardWithdrawalInfo.setVisibility(View.VISIBLE);
    }

        // запрос на вывод
    public void requestWithdrawalScr2(View v){
        // валидация суммы на вывод
        int withdrawalValue = 0;
        if(!(editWithdrawalValue.getText().toString().equals(""))){
            withdrawalValue  = Integer.valueOf(editWithdrawalValue.getText().toString()) ;
        }
        else {
            toastMsgLg("Введите сумму для вывода");
            return;
        }
       if(valueSum<withdrawalValue){
           toastMsgLg("Вы можете вывести не более "+String.valueOf(valueSum)+" €");
           return;
       }
        int valueCharsCard = editCardWithdrawal.getText().toString().length();

        // валидация номера кредитной карты
        if(editCardWithdrawal.getText().toString().equals("") || valueCharsCard != 16){
            toastMsgLg("Введите шестнадцатизначный номер банковской карты");
            return;
        }

        //после валидации введеных данных отправляем запрос на вывод средств
        postWithdrawal (editWithdrawalValue.getText().toString(),editCardWithdrawal.getText().toString());

    }

    //кнопка далее с окна условий вывода
    public void dialogWithdrawalInfoNext(View v){
        cardWithdrawalInfo.setVisibility(View.GONE);
        cardWithdrawalRequisites.setVisibility(View.VISIBLE);
    }
    //кнопка закрытия окна условий вывода
    public void dialogWithdrawalInfoClose(View v){
        cardWithdrawalInfo.setVisibility(View.GONE);
    }
    //кнопка закрытия окна ввода карты для вывода
    public void dialogWithdrawalCardClose(View v){
        cardWithdrawalRequisites.setVisibility(View.GONE);
    }

    //Запрос на снятие средств
    public void postWithdrawal (String withrawalValue, String card){
        String userId = Hawk.get("userIdHawk");
        ApiClient.getClient().postWithdrawalReq(userId,userEmail,userNameSurname,withrawalValue,card,formattedDate,"new").enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code() == 200 || response.code() == 204){
                    toastMsgLg(getResources().getString(R.string.activity_deposit_toast_suc));
                    cardWithdrawalRequisites.setVisibility(View.GONE);
                    //обновляем данные баланса
                    getUserBalance ();
                }else {
                    cardWithdrawalRequisites.setVisibility(View.GONE);
                    toastMsgLg(getResources().getString(R.string.activity_deposit_toast_dec));
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {

            }
        });
    }
}
