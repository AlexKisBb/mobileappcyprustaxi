package com.test.borya;

import adapters.OrdersAdapter;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import adapters.OrdersAdapterStory;
import models.OrderModel;
import models.OrdersObject;
import network.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import java.util.ArrayList;
import java.util.Collections;

public class StoryOrdersActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener  {
    Button btnProfit;
    Button btnExpense;
    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerViewAccepted;
    private OrdersAdapterStory mAdapter;
    private OrdersAdapterStory mAdapterAccepted;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.LayoutManager mLayoutManagerAccepted;
    ArrayList<OrderModel> arrayObjectsOrders= new ArrayList<>();
    ArrayList<OrderModel> arrayObjectsOrdersSorted = new ArrayList<>();
    ArrayList<OrderModel> arrayObjectsOrdersSortedAccepted = new ArrayList<>();
    SwipeRefreshLayout  swipeRefreshCreated;
    SwipeRefreshLayout  swipeRefreshAccepted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_orders);
        btnProfit = findViewById(R.id.btnProfit);
        btnExpense = findViewById(R.id.btnExpense);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_orders_created);
        mRecyclerViewAccepted = (RecyclerView) findViewById(R.id.recycler_orders_accepted);

        swipeRefreshCreated = (SwipeRefreshLayout) findViewById(R.id.swipe_container_create);
        swipeRefreshCreated.setOnRefreshListener(this);
        swipeRefreshAccepted= (SwipeRefreshLayout) findViewById(R.id.swipe_container_acc);
        swipeRefreshAccepted.setOnRefreshListener(this);
        recyclerInit ();
        getOrderss ();
        checkAccept ();
    }

    public void clickBack (View v){
        finish();
    }

    public void  clickBtnMyOrders (View v){
        btnProfit.setBackground(getResources().getDrawable(R.drawable.shape_btn_colored_left));
        btnExpense.setBackground(getResources().getDrawable(R.drawable.shape_white_right));
        btnProfit.setTextColor(getResources().getColor(R.color.colorWhite));
        btnExpense.setTextColor(getResources().getColor(R.color.colorPrimary));
        mRecyclerView.setVisibility(View.VISIBLE);
        mRecyclerViewAccepted.setVisibility(View.GONE);
        swipeRefreshCreated.setVisibility(View.VISIBLE);
        swipeRefreshAccepted.setVisibility(View.GONE);
    }
    public void  clickBtnAppliyOrders (View v){
        btnExpense.setBackground(getResources().getDrawable(R.drawable.shape_btn_colored_right));
        btnProfit.setBackground(getResources().getDrawable(R.drawable.shape_white_left));
        btnProfit.setTextColor(getResources().getColor(R.color.colorPrimary));
        btnExpense.setTextColor(getResources().getColor(R.color.colorWhite));
        mRecyclerView.setVisibility(View.GONE);
        mRecyclerViewAccepted.setVisibility(View.VISIBLE);
        swipeRefreshCreated.setVisibility(View.GONE);
        swipeRefreshAccepted.setVisibility(View.VISIBLE);
    }

    public void recyclerInit (){
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        mRecyclerViewAccepted.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManagerAccepted = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerViewAccepted.setLayoutManager(mLayoutManagerAccepted);

        // specify an adapter (see also next example)

        mAdapter = new OrdersAdapterStory(arrayObjectsOrdersSorted ,getApplicationContext());
        mAdapterAccepted = new OrdersAdapterStory(arrayObjectsOrdersSortedAccepted ,getApplicationContext());
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerViewAccepted.setAdapter(mAdapterAccepted);
    }
    public void clickToFullOrder(View v){
        //startActivity(new Intent(getApplicationContext(),FullOrderActivity.class).putExtra("extraCheckActivity", "extraStoryActivity"));
    }
    public void getOrderss (){
        ApiClient.getClient().getOrders().enqueue(new Callback<ArrayList<OrderModel>>() {
            @Override
            public void onResponse(Call<ArrayList<OrderModel>> call, Response<ArrayList<OrderModel>> response) {
                arrayObjectsOrders.clear();
                arrayObjectsOrders = response.body();
                Collections.reverse(arrayObjectsOrders);
                arrayObjectsOrdersSorted.clear();
                arrayObjectsOrdersSortedAccepted.clear();
                String ownerOrderHawk = Hawk.get("usernameHawk");
                String accepterIdHawk = Hawk.get("userIdHawk");
                for(int i = 0; i<arrayObjectsOrders.size(); i++){
                    if(arrayObjectsOrders.get(i).ownerOrder.equals(ownerOrderHawk)){
                        arrayObjectsOrdersSorted.add(arrayObjectsOrders.get(i));
                    }
                    if(arrayObjectsOrders.get(i).accepterId != null && arrayObjectsOrders.get(i).accepterId.equals(accepterIdHawk)){
                        arrayObjectsOrdersSortedAccepted.add(arrayObjectsOrders.get(i));
                       // toastMsgLg(accepterIdHawk);
                    }
                }
                //   toastMsg(String.valueOf(arrayObjectsOrders.size()));
                recyclerInit ();
            }
            @Override
            public void onFailure(Call<ArrayList<OrderModel>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onRefresh() {
        recyclerInit ();
        getOrderss ();
        // Отменяем анимацию обновления
        swipeRefreshCreated.setRefreshing(false);
        swipeRefreshAccepted.setRefreshing(false);
    }

    public void toastMsgLg(String msg){
        Toast toast = Toast.makeText(getApplicationContext(),
                msg, Toast.LENGTH_LONG);
        toast.show();
    }

    //проверка перехода после принятия заказа
    public void checkAccept (){
        Intent intent =  getIntent();
        if(intent.getStringExtra("intentname") != null && intent.getStringExtra("intentname").equals("accept")){
        btnExpense.callOnClick();
        }
    }
}
