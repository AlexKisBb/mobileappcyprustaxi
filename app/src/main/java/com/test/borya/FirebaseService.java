package com.test.borya;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import static com.android.volley.VolleyLog.TAG;

public class FirebaseService extends FirebaseMessagingService {
    public FirebaseService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            if (true) {
            } else {
            }
        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        String title = "";
        String msg = "";
        Intent intent = new Intent();
        Map<String, String> params = remoteMessage.getData();
        //определяем какой пуш пришел, задаем соответственные переходы и текст ---------------------
        switch (params.get("title")){
            case "addBalanceNotify":
                title = getResources().getString(R.string.notification_balance_replenishment_title);
                msg = getResources().getString(R.string.notification_balance_replenishment_body);
                intent = new Intent(this, BalanceActivity.class);
                break;
            case "regApproveNotify":
                title = getResources().getString(R.string.notification_user_moderation_title);
                msg = getResources().getString(R.string.notification_user_moderation_body);
                intent = new Intent(this, MainActivity.class);
                break;
            case "orderCanceledAccNotify":
                title = getResources().getString(R.string.notification_order_cancelled_title);
                msg = getResources().getString(R.string.notification_order_cancelled_body);
                intent = new Intent(this, StoryOrdersActivity.class);
                break;
            case "orderCanceledTimeNotify":
                title = getResources().getString(R.string.notification_order_cancelled_time_title);
                msg = getResources().getString(R.string.notification_order_cancelled_time_body);
                intent = new Intent(this, StoryOrdersActivity.class);
                break;
            case "orderModeratedNotify":
                    title = getResources().getString(R.string.notification_order_moderation_title);
                msg = getResources().getString(R.string.notification_order_moderation_body);
                intent = new Intent(this, StoryOrdersActivity.class);
                break;
            case "orderFinishedNotify":
                    title = getResources().getString(R.string.notification_order_finished_title);
                msg = getResources().getString(R.string.notification_order_finished_body);
                intent = new Intent(this, StoryOrdersActivity.class);
                break;
            case "orderAccNotify":
                    title = getResources().getString(R.string.notification_take_order_title);
                msg = getResources().getString(R.string.notification_take_order_body);
                intent = new Intent(this, StoryOrdersActivity.class);
                break;
            case "rejectWithdrawal":
                title = getResources().getString(R.string.notification_withdrawal_rej_title);
                msg = getResources().getString(R.string.notification_withdrawal_rej_body);
                intent = new Intent(this, BalanceActivity.class);
                break;
            case "succesWithdrawal":
                title = getResources().getString(R.string.notification_withdrawal_suc_title);
                msg = getResources().getString(R.string.notification_withdrawal_suc_body);
                intent = new Intent(this, BalanceActivity.class);
                break;
        }
        //переход в приложение ---------------------------------------------------------------------


        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,

                PendingIntent.FLAG_ONE_SHOT);
        //------------------------------------------------------------------------------------------

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);



        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_default_notif);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), "CHANNEL_ID");

        notificationBuilder.setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.drawable.ic_small_notif)
                .setLargeIcon(icon)
                .setTicker("Hearty365")
                .setPriority(Notification.PRIORITY_MAX) // this is deprecated in API 26 but you can still use for below 26. check below update for 26 API
                .setContentTitle(title)
                .setContentText(msg)
                .setContentInfo("Info")
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);







        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel("CHANNEL_ID",

                    "Channel human readable title",

                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationManager.createNotificationChannel(channel);

        }
        notificationManager.notify(1, notificationBuilder.build());

    }
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);
    }
}
