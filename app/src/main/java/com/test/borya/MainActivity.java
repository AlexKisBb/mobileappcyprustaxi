package com.test.borya;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.orhanobut.hawk.Hawk;

import androidx.cardview.widget.CardView;

import java.util.ArrayList;

import models.TokenModel;
import network.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import jp.wasabeef.blurry.Blurry;

public class MainActivity extends BaseActivity {
    CardView mainCard;
    CardView refreshPassCard;
    ImageView imgLable;
    boolean checkSoftKeyboard = false;
    TextInputEditText editTextEmail;
    TextInputEditText  editTextPass;
    TextInputEditText  editTextRefreshPass;
    String email;
    String pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Hawk.get("tokenPutSmsAct")!=(null)){
            // getProf (Hawk.get("tokenPutSmsAct").toString());
            getUserStatus ();
        }
        setContentView(R.layout.activity_main);


        mainCard = findViewById(R.id.mainCard);
        refreshPassCard = findViewById(R.id.cardRefreshPass);
        imgLable = findViewById(R.id.img_lable);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPass = findViewById(R.id.editTextPass);
        editTextRefreshPass = findViewById(R.id.editTextRefreshPass);

        attachKeyboardListeners();


    }

    public void clickEnter (View v){
        email = editTextEmail.getText().toString();
        pass = editTextPass.getText().toString();
        char[] charArrayEmail = email.toCharArray();
        char[] charArrayPass = pass.toCharArray();
        if(email.contains("@") & email.contains(".") & charArrayEmail.length>4 & charArrayPass.length>0){
            getOrderss ();

       }
        else {
            toastMsg(getResources().getString(R.string.main_activity_incorrect_email));
        }
    }
    public void clickRefresh (View v){
        String refreshPass = editTextRefreshPass.getText().toString();
        char[] charArrayrefreshPass = refreshPass.toCharArray();
        if(refreshPass.contains("@") & refreshPass.contains(".") & charArrayrefreshPass.length>4){
            mainCard.setVisibility(View.VISIBLE);
            refreshPassCard.setVisibility(View.GONE);
            if(checkSoftKeyboard){
                setMarginLeft(refreshPassCard,(int) getResources().getDimension(R.dimen.main_card_normal_margin_bottom));
            }
            toastMsgLg (getResources().getString(R.string.main_activity_send_refresh_email));
        }
        else {
            toastMsg(getResources().getString(R.string.main_activity_incorrect_only_email));
        }
    }

    public void openRefreshPass (View v){
        mainCard.setVisibility(View.GONE);
        refreshPassCard.setVisibility(View.VISIBLE);
        if(checkSoftKeyboard){
                setMarginLeft(refreshPassCard,(int) getResources().getDimension(R.dimen.main_card_small_margin_bottom));
        }
    }
    public void closeRefreshPass (View v){
        mainCard.setVisibility(View.VISIBLE);
        refreshPassCard.setVisibility(View.GONE);
        if(checkSoftKeyboard){
            setMarginLeft(refreshPassCard,(int) getResources().getDimension(R.dimen.main_card_normal_margin_bottom));
        }
    }

    public void clickForRegistration (View v){
        startActivity(new Intent(getApplicationContext(),EnterPersonalInformation.class));
    }
    @Override
    protected void onShowKeyboard(int keyboardHeight) {
        checkSoftKeyboard = true;
        setMarginLeft(mainCard,60);
        imgLable.setVisibility(View.GONE);
        if(refreshPassCard.getVisibility() == View.VISIBLE){
            setMarginLeft(refreshPassCard,(int) getResources().getDimension(R.dimen.main_card_small_margin_bottom));
        }
    }

    @Override
    protected void onHideKeyboard() {
        checkSoftKeyboard = false;
        setMarginLeft(mainCard,250);
        imgLable.setVisibility(View.VISIBLE);
        if(refreshPassCard.getVisibility() == View.VISIBLE){
            setMarginLeft(refreshPassCard,300);
        }
    }

    public static void setMarginLeft(View v, int btn) {
        ViewGroup.MarginLayoutParams params =
                (ViewGroup.MarginLayoutParams)v.getLayoutParams();
        params.setMargins(params.leftMargin, params.topMargin,
                params.rightMargin, btn);
    }

    public void  toastMsg (String msg){
        Toast toast = Toast.makeText(getApplicationContext(),
                msg, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void toastMsgLg(String msg){
        Toast toast = Toast.makeText(getApplicationContext(),
                msg, Toast.LENGTH_LONG);
        toast.show();
    }


    public void getOrderss (){
        ApiClient.getClient().postSingIn(email,pass).enqueue(new Callback <ArrayList<TokenModel>>() {
            @Override
            public void onResponse(Call <ArrayList<TokenModel>> call, Response <ArrayList<TokenModel>> response) {
                if(response.code() == 200){
                  //  ArrayList<TokenModel> arrayResponse = response.body();
                    Hawk.put("tokenPutSmsAct",response.body().get(0).token);
                    Hawk.put("ownerName",response.body().get(1).namesurname);
                    Hawk.put("ownerMail",response.body().get(1).email);
                    Hawk.put("usernameHawk",response.body().get(1).namesurname);
                    Hawk.put("userIdHawk",response.body().get(1).id);
                    Hawk.put("userAvatarHawk",response.body().get(1).userAvatar);

                    if(response.body().get(1).userStatus.equals("userOnModeration")){
                        toastMsgLg(getResources().getString(R.string.main_activity_accaunt_on_verification));
                    }
                    if(response.body().get(1).userStatus.equals("userIsBlocked")){
                        toastMsgLg(getResources().getString(R.string.main_activity_alert_block));
                    }
                    if(response.body().get(1).userStatus.equals("userModerated")){
                        startActivity(new Intent(getApplicationContext(),OrdersActivity.class));
                    }
               }
                if(response.code() == 401 || response.code() == 500){
                    toastMsgLg(getResources().getString(R.string.main_activity_incorrect_email));
                }
            }
            @Override
            public void onFailure(Call<ArrayList<TokenModel>> call, Throwable t) {
                toastMsgLg(t.toString());
            }
        });
    }

    public void getUserStatus (){
        String userIdForB = Hawk.get("userIdHawk");
        ApiClient.getClient().getOneUser(userIdForB).enqueue(new Callback<TokenModel>() {
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                String userStatus = response.body().userStatus;
                if(!userStatus.equals("userIsBlocked")){
                    startActivity(new Intent(getApplicationContext(),OrdersActivity.class));
                }
                else{
                    Hawk.put("tokenPutSmsAct",null);
                    toastMsgLg(getResources().getString(R.string.main_activity_alert_block));
                    //startActivity(new Intent(getApplicationContext(),MainActivity.class));
                }
            }
            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
                toastMsgLg(t.toString());
            }
        });
    }
}
