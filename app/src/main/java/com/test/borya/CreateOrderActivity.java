package com.test.borya;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import adapters.CustomAdapter;
import eventbus.DatePickerEvent;
import eventbus.TimePickerEvent;
import fragments.MyDatePickerFragment;
import fragments.MyTimePickerFragment;
import models.CitysLtLgObject;
import models.TokenModel;
import network.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.orhanobut.hawk.Hawk;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CreateOrderActivity extends AppCompatActivity {

    int AUTOCOMPLETE_REQUEST_CODE = 1;
//    EditText editTextFrom;
//    EditText editTextTo;
    EditText editTextDate;
    EditText editTextPuipleAmount;
    EditText editTextPrice;
    EditText editTextPriceOrder;
    EditText editDescription;
    EditText editPhoneClient;
    String checkFromTo;
    String dateContainer;
    CheckBox checkBox;
    CheckBox checkBoxBabySmallChair;
    CheckBox checkBoxMoneyFromTheCreator;
    String pointA;
    String pointB;
    String date;
    String amountPeople;
    String price;
    String priceOrder;
    String comment;
    String babyChair;
    String babySmallChair;
    String ownerOrder;
    String userAvatarLink;
    String firslLt;
    String firslLg;
    String secondLt;
    String secondLg;
    String phoneClient;
    String ownerId;
    static String status = "onmoderation";
    String unixMcOrder;

    Spinner spinnerFrom;
    EditText textFrom;
    Spinner spinnerTo;
    EditText textTo;

    ArrayList<String> citiesFrom = new ArrayList<>();
    ArrayList<String> citiesTo = new ArrayList<>();

    Integer citiesPos;
    AlertDialog.Builder alertDialog;

    boolean blMoneyFromTheCreator;

    String userBalance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_order);
        EventBus.getDefault().register(this);
      //  editTextFrom = findViewById(R.id.editTextFrom);Лимасол, Ларнака, Ларнака аэропорт, Пафос, Пафос аэропорт, Никосия, Ая-напа, Проторас
      //  editTextTo = findViewById(R.id.editTextTo);
        citiesFrom.add(getResources().getString(R.string.cr_or_activity_city_btn1));
        citiesFrom.add(getResources().getString(R.string.cr_or_activity_city_lim));
        citiesFrom.add(getResources().getString(R.string.cr_or_activity_city_lar));
        citiesFrom.add(getResources().getString(R.string.cr_or_activity_city_larair));
        citiesFrom.add(getResources().getString(R.string.cr_or_activity_city_paph));
        citiesFrom.add(getResources().getString(R.string.cr_or_activity_city_paphair));
        citiesFrom.add(getResources().getString(R.string.cr_or_activity_city_nic));
        citiesFrom.add(getResources().getString(R.string.cr_or_activity_city_ayanap));
        citiesFrom.add(getResources().getString(R.string.cr_or_activity_city_protor));

        citiesTo.add(getResources().getString(R.string.cr_or_activity_city_btn2));
        citiesTo.add(getResources().getString(R.string.cr_or_activity_city_lim));
        citiesTo.add(getResources().getString(R.string.cr_or_activity_city_lar));
        citiesTo.add(getResources().getString(R.string.cr_or_activity_city_larair));
        citiesTo.add(getResources().getString(R.string.cr_or_activity_city_paph));
        citiesTo.add(getResources().getString(R.string.cr_or_activity_city_paphair));
        citiesTo.add(getResources().getString(R.string.cr_or_activity_city_nic));
        citiesTo.add(getResources().getString(R.string.cr_or_activity_city_ayanap));
        citiesTo.add(getResources().getString(R.string.cr_or_activity_city_protor));

        spinnerFrom = findViewById(R.id.spinnerFrom);
        spinnerTo = findViewById(R.id.spinnerTo);

        textFrom = findViewById(R.id.textFrom);
        textTo = findViewById(R.id.textTo);

        editTextDate = findViewById(R.id.editTextDate);
        editTextPuipleAmount  = findViewById(R.id.editTextPuipleAmount);
        editTextPrice = findViewById(R.id.editTextPrice);
        editTextPriceOrder = findViewById(R.id.editTextPriceOrder);
        editDescription = findViewById(R.id.editDescription);
        checkBox = findViewById(R.id.checkBox);
        checkBoxBabySmallChair = findViewById(R.id.checkBoxBabySmallChair);
        checkBoxMoneyFromTheCreator = findViewById(R.id.checkBoxMoneyFromTheCreator);
        editPhoneClient = findViewById(R.id.editPhoneClient);
        ownerId = Hawk.get("userIdHawk");

        hideSoftKeyboard(editTextPuipleAmount);
        hideSoftKeyboard(editTextPrice);
        hideSoftKeyboard(editPhoneClient);
        hideSoftKeyboard(editDescription);

        // Initialize Places.
        Places.initialize(getApplicationContext(), "AIzaSyDDhUlgAVSRb4dLY7P5xmxStt4zf2KWHCU");
// Create a new Places client instance.
        PlacesClient placesClient = Places.createClient(this);

        spinnerCustomAdapterCityFrom();
        spinnerCustomAdapterCityTo();

        if(Hawk.get("checkOnboardingFullOrder") == null){
            showAlertOnboarding (getResources().getString(R.string.cr_or_activity_dialog));

        }

    }

    public void clickBack (View v){
        finish();
    }

    public void clickPlaceFrom (View v){

    }
    public void clickPlaceTo (View v){

    }

    public void spinnerCustomAdapterCityFrom(){
        ArrayList<String> arCity = new ArrayList<String>();
        for (int i=0;i<citiesFrom.size();i++){
            arCity.add(citiesFrom.get(i));
        }
        CustomAdapter adapter = new CustomAdapter(this,
                R.layout.custom_spinner_item, arCity);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFrom.setAdapter(adapter);
        spinnerFrom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                citiesPos = pos;
                CitysLtLgObject cityLtLg = new CitysLtLgObject();
                //Лимасол, Ларнака, Ларнака аэропорт, Пафос, Пафос аэропорт, Никосия, Ая-напа, Проторас
                switch (pos){
                    case 1:
                        textFrom.setVisibility(View.VISIBLE);
                        firslLt = cityLtLg.getLimassolLt();
                        firslLg = cityLtLg.getLimassolLg();
                        break;
                    case 2:
                        textFrom.setVisibility(View.VISIBLE);
                        firslLt = cityLtLg.getLarnacaLt();
                        firslLg = cityLtLg.getLarnacaLg();
                        break;
                    case 3:
                        textFrom.setVisibility(View.VISIBLE);
                        firslLt = cityLtLg.getLarnacaAirLt();
                        firslLg = cityLtLg.getLarnacaAirLg();
                        break;
                    case 4:
                        textFrom.setVisibility(View.VISIBLE);
                        firslLt = cityLtLg.getPaphosLt();
                        firslLg = cityLtLg.getPaphosLg();
                        break;
                    case 5:
                        textFrom.setVisibility(View.VISIBLE);
                        firslLt = cityLtLg.getPaphosAirLt();
                        firslLg = cityLtLg.getPaphosAirLg();
                        break;
                    case 6:
                        textFrom.setVisibility(View.VISIBLE);
                        firslLt = cityLtLg.getNicossiaLt();
                        firslLg = cityLtLg.getNicossiaLg();
                        break;
                    case 7:
                        textFrom.setVisibility(View.VISIBLE);
                        firslLt = cityLtLg.getAyaNapaLt();
                        firslLg = cityLtLg.getAyaNapaLg();
                        break;
                    case 8:
                        textFrom.setVisibility(View.VISIBLE);
                        firslLt = cityLtLg.getProtorasLt();
                        firslLg = cityLtLg.getProtorasLg();
                        break;
                }
                // Set adapter flag that something has been chosen
                CustomAdapter.flag = true;

            }}); }

    public void spinnerCustomAdapterCityTo(){
        ArrayList<String> arCity = new ArrayList<String>();
        for (int i=0;i<citiesTo.size();i++){
            arCity.add(citiesTo.get(i));
        }
        CustomAdapter adapter = new CustomAdapter(this,
                R.layout.custom_spinner_item, citiesTo);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTo.setAdapter(adapter);
        spinnerTo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {
                citiesPos = pos;
                citiesPos = pos;
                CitysLtLgObject cityLtLg = new CitysLtLgObject();
                //Лимасол, Ларнака, Ларнака аэропорт, Пафос, Пафос аэропорт, Никосия, Ая-напа, Проторас
                switch (pos){
                    case 1:
                        textTo.setVisibility(View.VISIBLE);
                        secondLt = cityLtLg.getLimassolLt();
                        secondLg = cityLtLg.getLimassolLg();
                        break;
                    case 2:
                        textTo.setVisibility(View.VISIBLE);
                        secondLt = cityLtLg.getLarnacaLt();
                        secondLg =  cityLtLg.getLarnacaLg();
                        break;
                    case 3:
                        textTo.setVisibility(View.VISIBLE);
                        secondLt = cityLtLg.getLarnacaAirLt();
                        secondLg =  cityLtLg.getLarnacaAirLg();
                        break;
                    case 4:
                        textTo.setVisibility(View.VISIBLE);
                        secondLt = cityLtLg.getPaphosLt();
                        secondLg =  cityLtLg.getPaphosLg();
                        break;
                    case 5:
                        textTo.setVisibility(View.VISIBLE);
                        secondLt = cityLtLg.getPaphosAirLt();
                        secondLg =  cityLtLg.getPaphosAirLg();
                        break;
                    case 6:
                        textTo.setVisibility(View.VISIBLE);
                        secondLt = cityLtLg.getNicossiaLt();
                        secondLg =  cityLtLg.getNicossiaLg();
                        break;
                    case 7:
                        textTo.setVisibility(View.VISIBLE);
                        secondLt = cityLtLg.getAyaNapaLt();
                        secondLg =  cityLtLg.getAyaNapaLg();
                        break;
                    case 8:
                        textTo.setVisibility(View.VISIBLE);
                        secondLt = cityLtLg.getProtorasLt();
                        secondLg =  cityLtLg.getProtorasLg();
                        break;
                }
              //  toastMsgLg(String.valueOf(citiesPos));
                // Set adapter flag that something has been chosen
                CustomAdapter.flag = true;
               // textTo.setVisibility(View.VISIBLE);
            }}); }
    //клик на поле ввода места откуда
//    public void clickPlaceFrom (View v){
//        checkFromTo = "from";
//        int AUTOCOMPLETE_REQUEST_CODE = 1;
//// Set the fields to specify which types of place data to
//// return after the user has made a selection.
//        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.ADDRESS,Place.Field.LAT_LNG);
//// Start the autocomplete intent.
//        Intent intent = new Autocomplete.IntentBuilder(
//                AutocompleteActivityMode.FULLSCREEN, fields)
//                .setCountry("cy")
//                .setTypeFilter(TypeFilter.ADDRESS)
//                .build(this);
//        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
//    }
//    public void clickPlaceTo(View v){
//        checkFromTo = "to";
//        int AUTOCOMPLETE_REQUEST_CODE = 1;
//// Set the fields to specify which types of place data to
//// return after the user has made a selection.
//        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.ADDRESS,Place.Field.LAT_LNG);
//// Start the autocomplete intent.
//        Intent intent = new Autocomplete.IntentBuilder(
//                AutocompleteActivityMode.FULLSCREEN, fields)
//                .setCountry("cy")
//                .setTypeFilter(TypeFilter.ADDRESS)
//                .build(this);
//        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
//    }
    //получение выбраного города
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
//            if (resultCode == RESULT_OK) {
//                Place place = Autocomplete.getPlaceFromIntent(data);
//                switch (checkFromTo){
//                    case "from":
//                      //  editTextFrom.setText(place.getAddress());
//                        firslLt = String.valueOf(place.getLatLng().latitude);
//                        firslLg = String.valueOf(place.getLatLng().longitude);
//                        break;
//                    case "to":
//                       // editTextTo.setText(place.getAddress());
//                        secondLt = String.valueOf(place.getLatLng().latitude);
//                        secondLg = String.valueOf(place.getLatLng().longitude);
//                        break;
//                }
//            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
//                // TODO: Handle the error.
//                Status status = Autocomplete.getStatusFromIntent(data);
//            //    Log.i(TAG, status.getStatusMessage());
//            } else if (resultCode == RESULT_CANCELED) {
//                // The user canceled the operation.
//            }
//        }
       }
    public void toastMsgLg(String msg){
        Toast toast = Toast.makeText(getApplicationContext(),
                msg, Toast.LENGTH_LONG);
        toast.show();
    }
    //выбор даты
    public void clickChoiseDate (View v){
        DialogFragment newFragment = new MyDatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "date picker");
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void pickDateMethod (DatePickerEvent event) {

        dateContainer = event.getEventString();
        pickTime();
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void pickTimeMethod (TimePickerEvent event) {
        long timestamp = System.currentTimeMillis();
        Date currentTime = Calendar.getInstance().getTime();
        timestamp =  currentTime.getTime();
        String toUnixTimeSt = dateContainer +" "+event.getEventString();

        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        Date dateForUnix = new Date();
        try {
            dateForUnix = df.parse(toUnixTimeSt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long epoch = dateForUnix.getTime();

        if(epoch>timestamp){
            unixMcOrder = String.valueOf(epoch-timestamp);
            Log.wtf("logstrttt",unixMcOrder);
            editTextDate.setText(dateContainer +" в "+event.getEventString());
        }
        else {
            toastMsgLg(getResources().getString(R.string.cr_or_activity_inc_time));
        }
    }
    public void pickTime(){
        DialogFragment newFragment = new MyTimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "time picker");
    }
    //клик для создание заказа
    public void clickCreateOrder(View v){
        if(!spinnerFrom.getSelectedItem().toString().equals(getResources().getString(R.string.cr_or_activity_city_btn1)) &&
                !spinnerTo.getSelectedItem().toString().equals(getResources().getString(R.string.cr_or_activity_city_btn2)) ){

            pointA = spinnerFrom.getSelectedItem().toString();
            pointB = spinnerTo.getSelectedItem().toString();
            if(textFrom.getText().toString() !=null){
                pointA =  pointA+" / "+textFrom.getText().toString();
            }
            if(textTo.getText().toString() !=null){
                pointB =  pointB+" / "+textTo.getText().toString();
            }
        }
        else {
            toastMsgLg(getResources().getString(R.string.cr_or_activity_toast_dir));
            return;
        }


        date = editTextDate.getText().toString();
        amountPeople = editTextPuipleAmount.getText().toString();
        price = editTextPrice.getText().toString();
        priceOrder = editTextPriceOrder.getText().toString();
        comment = editDescription.getText().toString();
        phoneClient = editPhoneClient.getText().toString();
        if(checkBox.isChecked()){
            babyChair = "yes";
        }else {
            babyChair = "no";
        }
        if(checkBoxBabySmallChair.isChecked()){
            babySmallChair = "yes";
        }else {
            babySmallChair = "no";
        }
        //сохраняем состояние флажка "деньги не у клиента"
        blMoneyFromTheCreator = checkBoxMoneyFromTheCreator.isChecked();

        ownerOrder = Hawk.get("usernameHawk");
        userAvatarLink = Hawk.get("userAvatarHawk");

        char[] charArraypointA = pointA.toCharArray();
        char[] charArraypointB = pointB.toCharArray();
        char[] charArraydate = date.toCharArray();
        char[] charArrayamountPeople = amountPeople.toCharArray();
        char[] charArrayprice = price.toCharArray();
        char[] charArraypriceOrder = priceOrder.toCharArray();
        char[] charArraycomment = comment.toCharArray();
        char[] charPhoneClient = phoneClient.toCharArray();

        if(charArraypointA.length>0&charArraypointB.length>0&charArraydate.length>0&charArrayamountPeople.length>0&
                charArrayprice.length>0 & charPhoneClient.length>0 & charArraypriceOrder.length>0){
            if(charArraycomment.length == 0){
                comment = " ";
            }
            getUserStatus ();
        }
        else {
            toastMsgLg(getResources().getString(R.string.cr_or_activity_toast_fillall)); }
    }
    //"application/x-www-form-urlencoded",
    public void postOrder(){
        ApiClient.getClient().postOrder(pointA,pointB,date,amountPeople,price,priceOrder,comment,babyChair,babySmallChair,ownerOrder,userAvatarLink,ownerId,
                firslLt,firslLg,secondLt,secondLg,status,phoneClient,unixMcOrder,blMoneyFromTheCreator).enqueue(new Callback<Void>(){
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                showAlertOk (getResources().getString(R.string.cr_or_activity_toast_success));

            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }

    public void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
    }
    public void showAlertOk (String  st){
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);
        alertDialog.setTitle(getResources().getString(R.string.cr_or_activity_appr_act));

        alertDialog.setMessage(st);

        alertDialog.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                // Код который выполнится после закрытия окна
                finish();
            }
        });
        // показываем Alert
        alertDialog.show();
    }
    public void showAlertRj (String  st){
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);
        alertDialog.setTitle(getResources().getString(R.string.cr_or_activity_appr_act));

        alertDialog.setMessage(st);

        alertDialog.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                // Код который выполнится после закрытия окна
               // finish();
            }
        });
        // показываем Alert
        alertDialog.show();
    }
    public void showAlertOnboarding (String  st){
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);
        alertDialog.setTitle(getResources().getString(R.string.cr_or_activity_appr_act));
        alertDialog.setMessage(st);
        alertDialog.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                // Код который выполнится после закрытия окна
                Hawk.put("checkOnboardingFullOrder",true);
            }
        });
        // показываем Alert
        alertDialog.show();
    }

    public void getUserStatus (){
        String userIdForB = Hawk.get("userIdHawk");
        ApiClient.getClient().getOneUser(userIdForB).enqueue(new Callback<TokenModel>() {
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                String userStatus = response.body().userStatus;
                if(!userStatus.equals("userIsBlocked")){
                    if(blMoneyFromTheCreator){
                        getUserBalance ();
                    }else {
                        postOrder();
                    }

                }
                else{
                    Hawk.put("tokenPutSmsAct",null);
                    toastMsgLg(getResources().getString(R.string.cr_or_activity_block));
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                }
            }
            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
                toastMsgLg(t.toString());
            }
        });
    }
    //получаем баланс пользователя для проверки на возможность "деньги не у клиента"
    public void getUserBalance (){
        String userId = Hawk.get("userIdHawk");
        ApiClient.getClient().getOneUser(userId).enqueue(new Callback<TokenModel>() {
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                userBalance = response.body().userBalance;
                int intBalance = Integer.valueOf(userBalance);
                int intTransferSumm = Integer.valueOf(editTextPriceOrder.getText().toString());
                if(intBalance>=intTransferSumm){
                    blockingFunds();
                }else {
                    showAlertRj (getResources().getString(R.string.cr_or_activity_toast_reject)+userBalance+"€");
                }

            }
            @Override
            public void onFailure(Call<TokenModel> call, Throwable t) {
                toastMsgLg(t.toString());
            }
        });
    }

    //блокировка средств
    public void blockingFunds(){
        String userId = Hawk.get("userIdHawk");
        ApiClient.getClient().postBlockingReq(userId,editTextPriceOrder.getText().toString()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code() == 200 || response.code() == 204){
                    postOrder();
                }else {
                    toastMsgLg("Ошибка связи с сервером");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable throwable) {

            }
        });
    }


}
