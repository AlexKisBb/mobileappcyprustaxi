package network;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import models.BalanceObject;
import models.OrderModel;
import models.TokenModel;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface Api {

    //отправка регистрационных данных  @Header("Content-Type") String contentType,
    @FormUrlEncoded
    @POST("testArrayOfObjects")
    Call<TokenModel> postRegistrationDataApiReq(
                                       @Field("email") String email,
                                       @Field("pass") String pass,
                                       @Field("namesurname") String namesurname,
                                       @Field("phone") String phone,
                                       @Field("carmark") String carmark,
                                       @Field("carmodel") String carmodel,
                                       @Field("userAvatar") String userAvatar,
                                       @Field("userStatus") String userStatus
    );

    //отправка регистрационных данных  @Header("Content-Type") String contentType,
    @FormUrlEncoded
    @POST("createOrder")
    Call<Void> postOrderApiReq(
            @Field("pointA") String pointA,
            @Field("pointB") String pointB,
            @Field("date") String date,
            @Field("amountPeople") String amountPeople,
            @Field("price") String price,
            @Field("priceOrder") String priceOrder,
            @Field("comment") String comment,
            @Field("babyChair") String babyChair,
            @Field("babySmallChair") String babySmallChair,
            @Field("ownerOrder") String ownerOrder,
            @Field("ownerAvatar") String ownerAvatar,
            @Field("ownerId") String ownerId,
            @Field("firslLt") String firslLt,
            @Field("firslLg") String firslLg,
            @Field("secondLt") String secondLt,
            @Field("secondLg") String secondLg,
            @Field("status") String status,
            @Field("phoneClient") String phoneClient,
            @Field("unixMcOrder") String unixMcOrder,
            @Field("blMoneyFromTheCreator") boolean blMoneyFromTheCreator
    );

    //получение списка заказов
    @GET("createOrder")
    Call<ArrayList<OrderModel>> getOrdersApiReq(
    );
    //получение конкретного заказов
    @GET("createOrder/{id}")
    Call<OrderModel> getOneOrderApiReq(
            @Path("id") String id
    );
    //получение конкретного пользователя
    @GET("testArrayOfObjects/{id}")
    Call<TokenModel> getOneUserApiReq(
            @Path("id") String id
    );

    //авторизация
    @FormUrlEncoded
    @POST("signIn")
    Call<ArrayList<TokenModel>> postSingInApiReq(
            @Field("email") String email,
            @Field("pass") String pass
    );
    //отправка фото пользователя
    @Multipart
    @POST("images")
    Call<TokenModel> uploadImage(@Part MultipartBody.Part file);
    //принятие заказа
    @FormUrlEncoded
    @PUT("createOrder/{id}/acceptance")
    Call<OrderModel> acceptOrder(
            @Path("id") String id,
            @Field("accepterId") String accepterId,
            @Field("accepter") String accepter,
            @Field("status") String status
    );
    //отмена заказа
    @FormUrlEncoded
    @PUT("createOrder/{id}/canceled")
    Call<OrderModel> cancellationOrder(
            @Path("id") String id,
            @Field("status") String status
    );

    //подтверждение заказа
    @FormUrlEncoded
    @PUT("createOrder/{id}/finishedOrder")
    Call<OrderModel> finishedOrder(
            @Path("id") String id,
            @Field("status") String status
    );

    // запись firebase токена
    @FormUrlEncoded
    @PUT("testArrayOfObjects/{id}/addFirebaseToken")
    Call<Void> addFirebaseToken(
            @Path("id") String id,
            @Field("fToken") String fToken
    );
    //создание запроса на пополнение баланса
    //авторизация
    @FormUrlEncoded
    @POST("createBalanceRequest")
    Call<Void> postBalanceReqApiReq(
            @Field("userId") String userId,
            @Field("userName") String userName,
            @Field("moneyValue") String moneyValue,
            @Field("statusBalance") String statusBalance,
            @Field("dateBalance") String dateBalance,
            @Field("accepterOrderBalance") String accepterOrderBalance,
            @Field("ownerOrderBalance") String ownerOrderBalance,
            @Field("postIdTransaction") String postIdTransaction
    );

    //получение массива заявок
    @GET("createBalanceRequest")
    Call <ArrayList<BalanceObject>> getBalanceReqApiReq(

    );

    //создание запроса на пополнение баланса
    //авторизация
    @FormUrlEncoded
    @POST("createBalanceRequest")
    Call<Void> postAcceptOrderChangeBalance(
            @Field("userId") String userId,
            @Field("userName") String userName,
            @Field("moneyValue") String moneyValue,
            @Field("statusBalance") String statusBalance,
            @Field("dateBalance") String dateBalance

    );

    //перечисление средств за заказ между пользователями
    @FormUrlEncoded
    @POST("changeUsersBalance")
    Call<Void> postTransferOrderPrice(
            @Field("accepterId") String accepterId,
            @Field("ownerId") String ownerId,
            @Field("valueCoastOrder") String valueCoastOrder
    );

    //создание заявки на вывод средств
    @FormUrlEncoded
    @POST("createWithdrawalRequest")
    Call<Void> postWithdrawalRequest(
            @Field("userId") String userId,
            @Field("userEmail") String userEmail,
            @Field("userName") String userName,
            @Field("userBalanceWithrawal") String userBalanceWithrawal,
            @Field("creditCard") String creditCard,
            @Field("requestDate") String requestDate,
            @Field("withdrawalStatus") String withdrawalStatus
    );
    //блокировка средств при создании заказа с условием "Деньги не у клиента"
    // запись firebase токена
    @FormUrlEncoded
    @POST("blokingFunds/{id}")
    Call<Void> postBlockOnCreateOrder(
            @Path("id") String id,
            @Field("transferPrice") String transferPrice
    );


}
