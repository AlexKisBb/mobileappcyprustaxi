package network;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import models.BalanceObject;
import models.OrderModel;
import models.TokenModel;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static final String BASE_URL="http://main-lb-748297709.us-east-2.elb.amazonaws.com/";

    private Api apiRequests;
    private static ApiClient getClient = null;
    private Retrofit retrofit;

    private ApiClient(){
        // Логирование для всех запросов к сети
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient networkClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(networkClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.apiRequests=retrofit.create(Api.class);
    }

    // Синглстон для ретрофит клиента
    public static ApiClient getClient() {
        if (getClient == null) {
            getClient = new ApiClient();
        }
        return getClient;
    }

    // отправка регистрационных данных String contentType, contentType,
    public Call<TokenModel> postRegistrationData ( String email,String pass,String namesurname,String phone,String carmark,String carmodel,String userAvatar,String userStatus){
        return apiRequests.postRegistrationDataApiReq(email,pass,namesurname,phone,carmark,carmodel,userAvatar,userStatus);
    }

    // создание заказа
    public Call<Void> postOrder (String pointA, String pointB, String date, String amountPeople, String price,String priceOrder, String comment, String babyChair,String babySmallChair, String ownerOrder,
                                 String firslLt, String firslLg, String secondLt, String secondLg,
                                 String status, String userAvatarLink,String ownerId, String phoneClient,
                                 String unixMcOrder,boolean blMoneyFromTheCreator){
        return apiRequests.postOrderApiReq(pointA,pointB,date,amountPeople,price,priceOrder,comment,
                babyChair,babySmallChair,ownerOrder,firslLt,firslLg,secondLt,secondLg,status,userAvatarLink,
                ownerId,phoneClient,unixMcOrder,blMoneyFromTheCreator);
    }

    // Получение списка заказов
    public Call<ArrayList<OrderModel>> getOrders(){
        return apiRequests.getOrdersApiReq();}

    //Получение конкретного заказа
    public Call<OrderModel> getOneOrder(String id){
        return apiRequests.getOneOrderApiReq(id);
    };

    //Получение конкретного пользователя
    public Call<TokenModel> getOneUser(String id){
        return apiRequests.getOneUserApiReq(id);
    };

    // отправка регистрационных данных String contentType, contentType,
    public Call<ArrayList<TokenModel>> postSingIn (String email, String pass){
        return apiRequests.postSingInApiReq(email,pass);
    }

    // Отправка фото юзера
    public Call<TokenModel> postUserAvatar(MultipartBody.Part file){
        return apiRequests.uploadImage(file);
    }

    // принятие заказа
    public Call<OrderModel> putAcceptOrder (String id, String accepterId, String accepter,String status){
        return apiRequests.acceptOrder(id,accepterId,accepter,status);}

    // изменение статуса заказа
    public Call<OrderModel> putStatusChangeOrder (String id,String status){
        return apiRequests.cancellationOrder(id,status);}

    //подтверждение выполнения заказа
    public Call<OrderModel> putFinishedOrder (String id,String status){
        return apiRequests.finishedOrder(id,status);}

    // отправка fToken
    public Call<Void> putfToken (String id,String fToken){
        return apiRequests.addFirebaseToken(id,fToken);}

    // Запрос на пополнение баланса
    public Call<Void> postBalanceReq(String userId,String userName,String moneyValue, String statusBalance,String dateBalance,String accepterOrderBalance,String ownerOrderBalance,String postIdTransaction){
        return apiRequests.postBalanceReqApiReq(userId,userName,moneyValue,statusBalance,dateBalance,accepterOrderBalance,ownerOrderBalance,postIdTransaction);
    }

    //Получение запросов на пополнение баланса
    public Call<ArrayList<BalanceObject>> getBalanceReq(){
        return apiRequests.getBalanceReqApiReq();
    };

    // перевод средств между пользователями за заказ
    public Call<Void> postTransferOderPrice (String accepterId, String ownerId, String valueCoastOrder){
        return apiRequests.postTransferOrderPrice(accepterId,ownerId,valueCoastOrder);
    }

    //Запрос на вывод средств
    public Call<Void> postWithdrawalReq (String userId, String userEmail, String userName, String userBalanceWithrawal, String creditCard, String requestDate,String withdrawalStatus){
        return apiRequests.postWithdrawalRequest(userId, userEmail, userName, userBalanceWithrawal, creditCard, requestDate, withdrawalStatus);
    }

    // локировка средств при создании заказа с условием "Деньги не у клиента"
    public Call<Void> postBlockingReq (String userId, String blockingSumm){
        return apiRequests.postBlockOnCreateOrder(userId,blockingSumm);
    }

}
